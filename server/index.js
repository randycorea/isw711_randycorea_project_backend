require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

// Database connection
const mongo_string = process.env.DATABASE_URL;

mongoose.connect(mongo_string, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const database = mongoose.connection;

database.on('error', (error) => {
    console.log('Database connection error:', error);
});

database.once('connected', () => {
    console.log('Database Connected');
});

console.log("Mongo URI:", process.env.DATABASE_URL);
console.log("JWT Secret:", process.env.JWT_SECRET);

// Parser for the request body (required for the POST and PUT methods)
app.use(express.json());

// Check for cors
app.use(cors({
    origin: '*',
    methods: "*"
}));

// Routes
app.use('/api/users', require('./routes/user'));
app.use('/api/playlists', require('./routes/playlist'));
app.use('/api/videos', require('./routes/video'));
app.use('/api/profiles', require('./routes/profile'));

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`The App Listening on Port ${PORT}`));
