const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    phone: { type: String, required: true },
    username: { type: String, unique: true, sparse: true }, // Utiliza 'sparse' si el campo puede ser nulo o no obligatorio
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    country: { type: String, required: true },
    birthDate: { type: Date, required: true },
    age: { type: Number, required: true }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
