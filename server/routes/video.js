const express = require('express');
const Video = require('../models/Video');
const Playlist = require('../models/Playlist');
const Profile = require('../models/Profile');
const router = express.Router();

// Crear un nuevo video
router.post('/', async (req, res) => {
    const { name, url, description, playlist } = req.body;

    try {
        const playlistExists = await Playlist.findById(playlist);
        if (!playlistExists) {
            return res.status(404).json({ message: 'Playlist not found' });
        }

        const video = new Video({
            name,
            url,
            description,
            playlist
        });
        await video.save();
        res.status(201).json(video);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

router.get('/', async (req, res) => {
    const { profileId } = req.query;
    try {
        let videos;
        if (profileId) {
            const profile = await Profile.findById(profileId).populate('playlists');
            if (profile && profile.age < 18) {
                const playlistIds = profile.playlists.map(playlist => playlist._id);
                videos = await Video.find({ playlist: { $in: playlistIds } }).populate('playlist');
            } else {
                const profilePlaylists = await Playlist.find({ associatedProfiles: profileId });
                const playlistIds = profilePlaylists.map(playlist => playlist._id);
                videos = await Video.find({ playlist: { $in: playlistIds } }).populate('playlist');
            }
        } else {
            videos = await Video.find({}).populate('playlist');
        }
        res.json(videos);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Obtener un video por ID
router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const video = await Video.findById(id).populate('playlist');
        if (!video) {
            return res.status(404).json({ message: 'Video not found' });
        }
        res.json(video);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Actualizar un video por ID
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const { name, url, description, playlist } = req.body;

    try {
        const video = await Video.findById(id);
        if (!video) {
            return res.status(404).json({ message: 'Video not found' });
        }

        if (playlist) {
            const playlistExists = await Playlist.findById(playlist);
            if (!playlistExists) {
                return res.status(404).json({ message: 'Playlist not found' });
            }
            video.playlist = playlist;
        }

        if (name) video.name = name;
        if (url) video.url = url;
        if (description) video.description = description;

        await video.save();
        res.status(200).json(video);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Eliminar un video por ID
router.delete('/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const video = await Video.findById(id);
        if (!video) {
            return res.status(404).json({ message: 'Video not found' });
        }

        await video.deleteOne();
        res.status(200).json({ message: 'Video deleted' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
