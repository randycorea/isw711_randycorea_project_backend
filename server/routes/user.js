const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const Profile = require('../models/Profile');
const router = express.Router();

router.post('/register', async (req, res) => {
    const { email, password, phone, pin, name, firstName, lastName, country, birthDate } = req.body;

    try {
        const existingUser = await User.findOne({ email });
        if (existingUser) {
            return res.status(400).json({ message: 'User already exists' });
        }

        const hashedPassword = await bcrypt.hash(password, 12);
        const hashedPin = await bcrypt.hash(pin, 12);
        const age = new Date().getFullYear() - new Date(birthDate).getFullYear();

        const newUser = new User({
            email,
            password: hashedPassword,
            phone,
            firstName,
            lastName,
            country,
            birthDate,
            age,
            username: email // Asegúrate de asignar un valor al campo 'username'
        });

        await newUser.save();

        const newProfile = new Profile({
            userId: newUser._id,
            name,
            pin: hashedPin,
            age
        });

        await newProfile.save();

        const token = jwt.sign({ email: newUser.email, id: newUser._id }, 'secret', { expiresIn: '1h' });

        res.status(201).json({ result: newUser, profile: newProfile, token });
    } catch (error) {
        res.status(500).json({ message: 'Something went wrong', error: error.message });
    }
});

router.post('/login', async (req, res) => {
    const { email, password } = req.body;

    try {
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(400).json({ message: 'Invalid email or password' });
        }

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            return res.status(400).json({ message: 'Invalid email or password' });
        }

        const token = jwt.sign({ userId: user._id }, 'secret', { expiresIn: '1h' }); // Use 'secret' directly for simplicity
        res.json({ token, user });
    } catch (err) {
        console.error('Error during login:', err);
        res.status(500).json({ message: err.message });
    }
});

router.post('/verify', async (req, res) => {
    const { token } = req.body;

    try {
        const decoded = jwt.verify(token, 'secret'); // Use 'secret' directly for simplicity
        const user = await User.findById(decoded.userId);
        if (!user) {
            return res.status(401).json({ message: 'User not found' });
        }
        res.json({ user });
    } catch (err) {
        console.error('Error during token verification:', err);
        res.status(401).json({ message: 'Token is not valid' });
    }
});

module.exports = router;
