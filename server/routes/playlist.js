const express = require('express');
const Playlist = require('../models/Playlist');
const Video = require('../models/Video');
const router = express.Router();

// Crear un nuevo playlist
router.post('/', async (req, res) => {
    const { name, associatedProfiles, owner } = req.body;

    try {
        const playlist = new Playlist({
            name,
            associatedProfiles,
            owner
        });
        await playlist.save();
        res.status(201).json(playlist);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Obtener todas las playlists
router.get('/', async (req, res) => {
    try {
        const playlists = await Playlist.find({})
            .populate('associatedProfiles')
            .lean();
        
        for (const playlist of playlists) {
            const videosCount = await Video.countDocuments({ playlist: playlist._id });
            playlist.videosCount = videosCount;
        }

        res.json(playlists);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Obtener una playlist por ID
router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const playlist = await Playlist.findById(id)
            .populate('associatedProfiles')
            .lean();
        
        if (!playlist) {
            return res.status(404).json({ message: 'Playlist not found' });
        }

        const videosCount = await Video.countDocuments({ playlist: playlist._id });
        playlist.videosCount = videosCount;

        res.json(playlist);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Actualizar una playlist por ID
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const { name, associatedProfiles, owner } = req.body;

    try {
        const playlist = await Playlist.findById(id);
        if (!playlist) {
            return res.status(404).json({ message: 'Playlist not found' });
        }

        if (name) playlist.name = name;
        if (associatedProfiles) playlist.associatedProfiles = associatedProfiles;
        if (owner) playlist.owner = owner;

        await playlist.save();
        res.status(200).json(playlist);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Eliminar una playlist por ID
router.delete('/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const playlist = await Playlist.findById(id);
        if (!playlist) {
            return res.status(404).json({ message: 'Playlist not found' });
        }

        await playlist.deleteOne(); // Cambiado de playlist.remove() a playlist.deleteOne()
        res.status(200).json({ message: 'Playlist deleted' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
