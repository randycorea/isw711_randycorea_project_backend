const express = require('express');
const bcrypt = require('bcryptjs');
const Profile = require('../models/Profile');
const router = express.Router();

router.get('/user/:userId', async (req, res) => {
    const { userId } = req.params;
    try {
        const profiles = await Profile.find({ userId });
        res.status(200).json(profiles);
    } catch (error) {
        res.status(404).json({ message: 'Profiles not found' });
    }
});

router.post('/verify-pin', async (req, res) => {
    const { profileId, pin } = req.body;
    try {
        const profile = await Profile.findById(profileId);
        if (!profile) {
            return res.status(404).json({ message: 'Profile not found' });
        }
        const isMatch = await bcrypt.compare(pin, profile.pin);
        if (isMatch) {
            res.status(200).json({ message: 'PIN verified' });
        } else {
            res.status(400).json({ message: 'Incorrect PIN' });
        }
    } catch (error) {
        res.status(500).json({ message: 'Something went wrong' });
    }
});

// Agregar la ruta para crear un perfil
router.post('/', async (req, res) => {
    const { userId, name, pin, age, avatar } = req.body;
    try {
        const hashedPin = await bcrypt.hash(pin, 12);
        const newProfile = new Profile({ userId, name, pin: hashedPin, age, avatar });
        await newProfile.save();
        res.status(201).json(newProfile);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

// Agregar la ruta para actualizar un perfil
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const { name, pin, age, avatar } = req.body;
    try {
        const profile = await Profile.findById(id);
        if (!profile) {
            return res.status(404).json({ message: 'Profile not found' });
        }
        if (pin) {
            profile.pin = await bcrypt.hash(pin, 12);
        }
        if (name) profile.name = name;
        if (age) profile.age = age;
        if (avatar) profile.avatar = avatar;
        await profile.save();
        res.status(200).json(profile);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

// Agregar la ruta para eliminar un perfil
router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const profile = await Profile.findById(id);
        if (!profile) {
            return res.status(404).json({ message: 'Profile not found' });
        }
        await Profile.deleteOne({ _id: id });
        res.status(200).json({ message: 'Profile deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

module.exports = router;
